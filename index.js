const http = require('http');
const port = 3000;
const server = http.createServer((request,response)=>{

	if(request.url == '/login'){
		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end("Welcome to login page");
	}
	else{
		response.writeHead(404,{'Content-Type':'text/plain'});
		response.end("The page ou are looking cannot be found");
	}
})
server.listen(port);
console.log("Server is running successfully")